#!/usr/bin/env python3
"""Binary precompiler for only sourced packages.

Usage:
```
mkdir -p .cache/dephell
python3 << EOF
from dephell.converters import CONVERTERS
if True:
  resolver=CONVERTERS['setuppy'].load_resolver(path='setup.py')
  resolver.graph.fast_apply()
  with open('./reqs.txt', 'wt') as f:
    for dep in resolver.graph:
      if not (dep.envs - {'main'}):
        f.write(f'{dep}\n')
EOF
./precompile_source_deps.py ./reqs.txt \
  --output-dir /tmp/precompiled \
  --skip-another-python-version \
  | xargs echo
```
"""
import asyncio
import attr
from concurrent.futures import ThreadPoolExecutor
from contextlib import redirect_stdout
from datetime import datetime
import os
import re
from shutil import move
from subprocess import CalledProcessError, run
import sys
from tempfile import TemporaryDirectory
from urllib.parse import urlparse

import aiohttp
from anyio import aopen
import click
from dephell.actions import get_resolver as get_resolver_from_content
from dephell.cache import BinCache, JSONCache, TextCache
from dephell.config import config as dephell_config
from dephell.exceptions import PackageNotFoundError
from dephell.models import Release
from dephell.networking import requests_session
from dephell.repositories import get_repo
from dephell_specifier import RangeSpecifier
from dictdiffer import diff as dict_diff
from packaging.markers import Marker as PackagingMarker
from pyunpack import Archive

pypi = get_repo(name='pypi')
loop = asyncio.get_event_loop()
dephell_pool = ThreadPoolExecutor(max_workers=5)
dephell_config.attach_env_vars()
RE_SYSTEM_ARCH = re.compile(r'.*-([^-]+)\.(exe|whl)$')


@attr.s(hash=False, eq=False, order=False)
class ReleaseWithUrls(Release):
    source_urls = attr.ib(factory=tuple, repr=False)
    binary_urls = attr.ib(factory=dict, repr=False)

    @classmethod
    def from_response(cls, name, version, info, extra=None):
        (*_, latest) = info
        python = latest['requires_python']
        if python is not None:
            python = RangeSpecifier(python)

        urls = tuple(rel['url'] for rel in info)
        source_urls = tuple(
            rel['url']
            for rel in info
            if rel['python_version'] == 'source')
        binary_urls = {}
        for rel in info:
            if rel['url'] in source_urls:
                continue
            if rel['packagetype'] == 'bdist_egg':
                # ignore eggs because it's deprecated but there are in metadatas
                continue
            key = (rel['python_version'], RE_SYSTEM_ARCH.match(rel['filename']).group(1))
            if key in binary_urls:
                binary_urls[key].add(rel['url'])
            else:
                binary_urls[key] = {rel['url']}

        return cls(
            raw_name=name,
            version=version,
            time=datetime.strptime(latest['upload_time'], '%Y-%m-%dT%H:%M:%S'),
            python=python,
            hashes=tuple(rel['digests']['sha256'] for rel in info),
            urls=urls,
            source_urls=source_urls,
            binary_urls=binary_urls,
            extra=extra,
        )


def get_releases_with_urls(self, dep):
    cache = JSONCache(
        'warehouse-api', urlparse(self.url).hostname,
        'releases', dep.base_name,
        ttl=dephell_config['cache']['ttl'],
    )
    data = cache.load()
    if data is None:
        url = '{url}{name}/json'.format(url=self.url, name=dep.base_name)
        with requests_session() as session:
            response = session.get(url, auth=self.auth)
        if response.status_code == 404:
            raise PackageNotFoundError(package=dep.base_name, url=url)
        data = response.json()
        cache.dump(data)
    elif isinstance(data, str) and data == '':
        return ()

    # update info for dependency
    self._update_dep_from_data(dep=dep, data=data['info'])

    # init releases
    releases = []
    prereleases = []
    for version, info in data['releases'].items():
        # ignore version if no files for release
        if not info:
            continue
        release = ReleaseWithUrls.from_response(
            name=dep.base_name,
            version=version,
            info=info,
            extra=dep.extra,
        )

        # filter prereleases if needed
        if release.version.is_prerelease:
            prereleases.append(release)
            if not self.prereleases and not dep.prereleases:
                continue

        releases.append(release)

    # special case for black: if there is no releases, but found some
    # prereleases, implicitly allow prereleases for this package
    if not releases and prereleases:
        releases = prereleases

    releases.sort(reverse=True)
    return tuple(releases)


async def cache_load(cache):
    return await loop.run_in_executor(
        dephell_pool,
        cache.load)


async def cache_dump(cache, value):
    return await loop.run_in_executor(
        dephell_pool,
        cache.dump,
        value)


def build_dep(source_archive, output_dir):
    prefix = f'tmp_{os.path.basename(source_archive)}_'
    with TemporaryDirectory(prefix=prefix) as unpack_dir:
        Archive(source_archive).extractall(unpack_dir)
        (_, [unpacked_dirname], _) = next(os.walk(unpack_dir))
        unpacked_dir = os.path.join(unpack_dir, unpacked_dirname)
        dist = os.path.join(unpacked_dir, 'dist')
        try:
            proc = run(
                ['python3', 'setup.py', 'bdist_wheel'],
                capture_output=True,
                cwd=unpacked_dir,
                env={**os.environ, 'LC_ALL': 'POSIX'},
                check=True)
        except CalledProcessError as bdist_er:
            # FIXME: may fail if legacy package have at least one open (>=) requirement
            try:
                proc = run(
                    [
                        'python3', '-m', 'pip', 'wheel',
                        source_archive, '--wheel-dir', dist
                    ],
                    capture_output=True,
                    check=True)
            except CalledProcessError as pip_er:
                print(bdist_er.stdout.decode())
                with redirect_stdout(sys.stderr):
                    print(bdist_er.stderr.decode())
                print(pip_er.stdout.decode())
                with redirect_stdout(sys.stderr):
                    print(pip_er.stderr.decode())
                raise
        precompiled_dep_filenames = next(os.walk(dist))[2]
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        for dep_filename in precompiled_dep_filenames:
            move(os.path.join(dist, dep_filename),
                 os.path.join(output_dir, dep_filename))
    return precompiled_dep_filenames


async def build_deps(deps, package_slug=None, output_dir=None):
    dep_by_name = {dep.name: dep for dep in deps}
    expected_reqs = {
        dep_name: str(dep.group.best_release.version)
        for (dep_name, dep) in dep_by_name.items()
    }
    cached_reqs = {}
    precompiled_versions = None
    if package_slug:
        precompiled_versions_cache = TextCache(
            'ci-cd', 'reqs', 'versions', package_slug)
        precompiled_versions = await cache_load(precompiled_versions_cache)
    if not precompiled_versions:
        precompiled_versions = ()
    for line in precompiled_versions:
        if not line:
            continue
        n, v = line.split('==')
        cached_reqs[n] = v
    async with aiohttp.ClientSession() as web_session:
        for diff_action in dict_diff(cached_reqs, expected_reqs, expand=True):
            action, dep_name, action_data = diff_action
            if action == 'change':
                old, new = action_data
            elif action == 'add':
                old = None
                [(dep_name, new)] = action_data
            elif action == 'remove':
                new = None
                [(dep_name, old)] = action_data
            else:
                raise RuntimeError(f'Unknown dictdiffer action: {action}')
            with redirect_stdout(sys.stderr):
                print(dict(
                    dep_name=dep_name,
                    old_version=old,
                    new_version=new,
                    sources=dep_by_name[dep_name].constraint.sources
                ))
            if new:
                precompiled_dep_files = await cache_load(
                    BinCache('ci-cd', 'deps', 'files', dep_name, new))
                if not precompiled_dep_files:
                    releases = get_releases_with_urls(
                        pypi, dep_by_name[dep_name])
                    [new_release] = [
                        rel
                        for rel in releases
                        if str(rel.version) == new]
                    [source_url] = new_release.source_urls
                    (*_, source_filename) = source_url.split('/')
                    async with web_session.get(source_url) as response:
                        source_body = await response.read()
                    with TemporaryDirectory() as tempdir_path:
                        source_path = os.path.join(
                            tempdir_path, source_filename)
                        async with await aopen(source_path, 'wb') as f:
                            await f.write(source_body)
                        build_dir = os.path.join(
                            tempdir_path, f'{source_filename}_build')
                        os.mkdir(build_dir)
                        precompiled_dep_filenames = \
                            await loop.run_in_executor(
                                None, build_dep,
                                source_path, tempdir_path)
                        precompiled_dep_files = {}
                        for dep_filename in precompiled_dep_filenames:
                            dep_filepath = os.path.join(
                                tempdir_path, dep_filename)
                            async with await aopen(dep_filepath, 'rb') as f:
                                precompiled_dep_files[dep_filename] = \
                                    await f.read()
                    await cache_dump(
                        BinCache('ci-cd', 'deps', 'files', dep_name, new),
                        precompiled_dep_files)
            if old:
                await cache_dump(
                    BinCache('ci-cd', 'deps', 'files', dep_name, old),
                    '')
    built_deps = set()
    for dep_name, dep_version in expected_reqs.items():
        precompiled_dep_files = await cache_load(
            BinCache('ci-cd', 'deps', 'files', dep_name, dep_version))
        for dep_filename, dep_file in precompiled_dep_files.items():
            dep_filepath = os.path.join(output_dir, dep_filename)
            async with await aopen(dep_filepath, 'wb') as f:
                await f.write(dep_file)
            built_deps.add(dep_filename)
    if package_slug:
        await cache_dump(
            precompiled_versions_cache,
            [
                f'{name}=={version}'
                for (name, version) in expected_reqs.items()])
    return built_deps


def latest_have_only_source_urls(dep, skip_another_python_version=False):
    assert not dep.group.extra
    assert len(dep.group.releases) == 1
    releases = get_releases_with_urls(pypi, dep)
    (*_, latest) = sorted(dep.constraint.filter(releases))
    actual_py_versions = {
        'py3',
        'py2.py3',
        'cp{}{}'.format(*sys.version_info)
    }
    def satisfy(py_ver, sys_arch):
        if not any(arch in sys_arch for arch in ('lin', 'any')):
            return False
        if not skip_another_python_version:
            return True
        if py_ver in actual_py_versions:
            return True
    binary_urls = [
        url
        for ((py_ver, sys_arch), urls) in latest.binary_urls.items()
        for url in urls
        if satisfy(py_ver, sys_arch)]
    return latest.source_urls and not binary_urls


@click.command()
@click.argument('requirements', nargs=-1)
@click.option('-o', '--output-dir', default=None,
              help='Output dir (defaults to current working dir).')
@click.option('--skip-another-python-version', is_flag=True,
              help='Skip binaries compiled for another python versions.')
@click.option('--package-slug',
              help='Package slug for caching, for example name and version.',
              default=None)
def generate_dep_binaries(requirements, output_dir,
                          skip_another_python_version,
                          package_slug):
    test_files = [os.path.exists(req) for req in requirements]
    if all(test_files):
        def get_resolver_from_files(reqs):
            content = ''
            for req in reqs:
                with open(req, 'rt') as f:
                    content += f.read()
                if not content.endswith('\n'):
                    content += '\n'
            return get_resolver_from_content(reqs=content.split('\n'))
        get_resolver = get_resolver_from_files
    elif any(test_files):
        raise RuntimeError(
            "Provide either requirements.txt's or packages strings.")
    else:
        get_resolver = get_resolver_from_content
    if output_dir is None:
        output_dir = os.getcwd()
    output_dir = os.path.abspath(os.path.expanduser(output_dir))
    resolver = get_resolver(reqs=requirements)
    with redirect_stdout(sys.stderr):
        result = resolver.resolve()
    assert result
    children_wo_binaries = []
    for dep in resolver.graph:
        if dep.marker and not PackagingMarker(str(dep.marker)).evaluate():
            # TODO: https://github.com/dephell/dephell/blob/0f7579be99b7c8014c5fd77aa09d811dab9cf63c/dephell/controllers/_resolver.py#L186-L209
            continue
        if not latest_have_only_source_urls(
            dep,
            skip_another_python_version=skip_another_python_version
        ):
            continue
        children_wo_binaries.append(dep)
    built_deps = loop.run_until_complete(build_deps(
        children_wo_binaries,
        package_slug=package_slug,
        output_dir=output_dir
    ))
    print('\n'.join(sorted(built_deps)))


if __name__ == '__main__':
    generate_dep_binaries()
